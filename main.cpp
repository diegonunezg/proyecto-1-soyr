#include <semaphore.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <thread>
#include <vector>
#include <fstream>
#include <iostream>

using namespace std;

// Variables asociadas a productores y consumidores.
int NP, NC, BC, NPP, NCC;

// Buffer para almacenar los productos.
vector<string> buffer;

// Se declaran los semáforos a utilizar.
sem_t exc_mutua; // Semáforo para la modificación del buffer.
sem_t escribir_p; // Semáforo para la escritura en el archivo de texto de los productores.
sem_t escribir_c; // Semáforo para la escritura en el archivo de texto de los consumidores.

// Se crean 2 instancias de tipo ofstream para la escritura de archivos.
ofstream gestor_archivo_p;
ofstream gestor_archivo_c;

// Esta función se encarga de escribir en el archivo los logs de ejecución del programa.
void escribir_archivo(ofstream *gestor_archivo, string path, string text, sem_t *escribir) {
    sem_wait(escribir); // Escribir en el archivo es una operación crítica
    gestor_archivo->open(path, ios_base::app);
    *gestor_archivo << text << endl;
    gestor_archivo->close();
    sem_post(escribir); // Se libera el acceso a la escritura en el archivo
}

/* Esta función es ejecutada por cada una de las hebras productoras */
void productor(int id){

    int prod_iter = 1; // Cada hebra lleva la cuenta de cuantos productos ha producido
    for (int i = 0; i < NPP; i++){
        
        /* SE GENERA LA CADENA Y SE ESCRIBE EN EL ARCHIVO */
        string cadena = to_string(id)+"_"+to_string(prod_iter);
        string mensaje = to_string(id) + " Generó " + cadena;
        escribir_archivo(&gestor_archivo_p, "productores.txt", mensaje, &escribir_p);

        /* Se intenta insertar en el buffer */
        sem_wait(&exc_mutua); // Añadir la cadena es una operación crítica
        if (buffer.end()-buffer.begin() < BC) {  // Si el buffer no está lleno
            
            buffer.push_back(cadena);
            sem_post(&exc_mutua); // Se libera el acceso al buffer

            // Se genera el mensaje a escribir en el log
            string mensaje = to_string(id) + " " + cadena + " Inserción exitosa";
            escribir_archivo(&gestor_archivo_p, "productores.txt", mensaje, &escribir_p);

            prod_iter++; // El productor aumenta en uno la cantidad de productos
            sleep(rand()%6); // Duerme una cantidad de segundos al azar entre 0 y 5

        } else { // Si el buffer está lleno
            sem_post(&exc_mutua); // Se libera el acceso al buffer

            // Al haber un error de inserción, se genera el mensaje a escribir en el log
            string mensaje = to_string(id) + " " + cadena + " Buffer lleno - error de inserción";
            escribir_archivo(&gestor_archivo_p, "productores.txt", mensaje, &escribir_p);

            i--; // Como no se hizo la inserción, se decrementa el iterador para volverlo a intentar 
            sleep(rand()%4); // Duerme una cantidad de segundos al azar entre 0 y 4
        }
    }

    // Cuando el productor termina, se genera el mensaje a escribir en el log
    string mensaje = "PRODUCTOR " + to_string(id) + " HA TERMINADO";
    escribir_archivo(&gestor_archivo_p, "productores.txt", mensaje, &escribir_p);
}

/* Esta función es ejecutada por cada una de las hebras consumidoras */
void consumidor(int id) {

    int cons_iter = 1; // Cada consumidor lleva la cuenta de cuantos productos va a consumir

    for (int i = 0; i < NCC; i++){
        /* SE GENERA LA CADENA Y SE ESCRIBE EN EL ARCHIVO */
        string mensaje = to_string(id) + " intentando eliminar el elemento del búfer";
        escribir_archivo(&gestor_archivo_c, "consumidores.txt", mensaje, &escribir_c);

        /* Se intenta consumir un producto del buffer */
        sem_wait(&exc_mutua); // Eliminar una cadena del buffer es una operación crítica
        if (!buffer.empty()) { // Si el buffer no esta vacio, se consume el primer elemento

            string cadena = buffer.front();
            buffer.erase(buffer.begin());
            sem_post(&exc_mutua); // Se libera el acceso al buffer

            // Se genera el mensaje a escribir en el log de consumidores
            string mensaje = to_string(id) + " " + cadena + " eliminado con exito";
            escribir_archivo(&gestor_archivo_c, "consumidores.txt", mensaje, &escribir_c);

            cons_iter++; // Se incrementa la cantidad de elementos consumidos
            sleep(rand()%6); // Duerme una cantidad de segundos al azar entre 0 y 5

        } else { // Si el buffer esta vacío, el consumidor se bloquea hasta que se inserte un elemento
            sem_post(&exc_mutua); // Se libera el acceso al buffer

            while (true) { // Para bloquear al consumidor, entra en un ciclo hasta que el buffer no este vacío
                if (!buffer.empty())
                    break;
            }
            i--; // Se decrementa el iterador, para tratar de consumir nuevamente
        }
    }

    // Cuando el consumidor termina, se genera el mensaje a escribir en el log
    string mensaje = "CONSUMIDOR " + to_string(id) + " HA TERMINADO";
    escribir_archivo(&gestor_archivo_c, "consumidores.txt", mensaje, &escribir_c);
}

// La función main del programa recibe parametros de la línea de comandos 
int main(int argc, char* argv[]) {

    // Si la cantidad de parametros no es la correcta, el programa no puede continuar
    if (argc != 6) {
        cout << "La cantidad de argumentos ingresados no es correcta." << endl;
        cout << "\tEl formato debe ser:\t./<exe> NP NC BC NPP NCC" << endl;
        cout << "Donde <exe> es el nombre del ejecutable, NP y NC el número de productores y consumidores respectivamente," << endl;
        cout << "BC corresponde al tamaño del buffer, NPP y NCC son la cantidad de productos a producir y a consumir por cada productor/consumidor." << endl;

        return -1; // El programa se detiene
    }

    // Se castea los parámetros ingresados a int, y se inicializan las variables
    NP = atoi(argv[1]);
    NC = atoi(argv[2]);
    BC = atoi(argv[3]);
    NPP = atoi(argv[4]);
    NCC = atoi(argv[5]);

    // Si alguno de los parámetros ingresados es menor o igual a 0, el programa no puede continuar
    if (NP <= 0 || NC <= 0 || BC <= 0 || NPP <= 0 || NCC <= 0) {
        cout << "Los valores ingresados deben ser positivos." << endl;
        return -1; // El programa se detiene
    }

    // Si la cantidad a producir es menor que la cantidad a consumir, el programa no puede continuar
    if (NP*NPP < NC*NCC) {
        cout << "La cantidad a producir será menor a la demandada." << endl;
        return -1; // El programa se detiene
    // Si la cantidad a producir excede la cantidad a consumir más el espacio en el buffer, el programa no puede continuar
    } else if (NP*NPP > NC*NCC + BC) {
        cout << "La cantidad a producir excede a la demanda y la capacidad en stock." << endl;
        return -1; // El programa se detiene
    }

    cout << "El programa esta en ejecución, el tiempo que demore varia dependiendo de los parametros iniciales." << endl;
    cout << "Espere a que el programa termine." << endl;

    // Semilla para la generación de númerps aleatoreos
    srand(time(NULL));

    // Se inicializan los semáforos
    sem_init(&exc_mutua, 0, 1);
    sem_init(&escribir_p, 0, 1);
    sem_init(&escribir_c, 0, 1);

    // Se crean los archivos de texto si es que no existen, y se limpia su contenido en caso de que existan
    gestor_archivo_p.open("productores.txt");
    gestor_archivo_p.close();
    gestor_archivo_c.open("consumidores.txt");
    gestor_archivo_c.close();

    // Se crean arreglos para almacenar las hebras productoras y consuidoras
    thread productores[NP];
    thread consumidores[NC];

    // Se crean las hebras productoras
    for (int i = 0; i < NP; i++) {
        productores[i] = thread(productor, i+1);
        string mensaje = "PRODUCTOR " + to_string(i+1) + " CREADO";
        escribir_archivo(&gestor_archivo_p, "productores.txt", mensaje, &escribir_p);
    }

    // Se crean las hebras consumidoras
    for (int i = 0; i < NC; i++) {
        consumidores[i] = thread(consumidor, i+1);
        string mensaje = "CONSUMIDOR " + to_string(i+1) + " CREADO";
        escribir_archivo(&gestor_archivo_c, "consumidores.txt", mensaje, &escribir_c);
    }

    // Se espera a que todas las hebras productoras terminen su ejecución
    for(int i = 0; i < NP; i++) {
        productores[i].join();
    }
    // Se espera a que todas las hebras consumidoras terminen su ejecución
    for(int i = 0; i < NC; i++) {
        consumidores[i].join();
    }

    // Se destruyen los semáforos
    sem_destroy(&exc_mutua);
    sem_destroy(&escribir_p);
    sem_destroy(&escribir_c);

    // Una vez que las hebras han terminado y se ha liberado la memoria de los semáforos, el programa finaliza su ejecución
    cout << "El programa ha finalizado. Consulte los archivos de texto para ver el resultado." << endl;
    return 0;
}
