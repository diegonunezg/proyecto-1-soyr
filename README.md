# Proyecto 1 SOyR

###### Por: Diego Núñez

---

### Planteamiento del problema
El problema propuesto, tiene por objetivo, simular una situación productor-consumidor en la que los primeros producen elementos y los segundos consumen estos elementos. En un contexto informático-computacional, estos actores (productores/consumidores), son representados por hebras que ejecutan la función respectiva. El problema principal, y el objetivo del proyecto, radica en asegurar una correcta interacción entre los recursos compartidos y los diversos procesos en ejecución paralela. Solo un consumidor o un productor pueden acceder al buffer a la vez, ya que de lo contrario, se puede perder información o corromper la misma. Para lograr esto, se emplean semáforos, y su función se discutirá más adelante.

### Objetivos
- Implementar solución al problema productor-consumidor utilizando hebras y semáforos.  
- Simular una situación de producción y consumo de productos.

### Consideración de un buen funcionamiento del programa
- Luego de compilar, para ejecutar el programa se deben pasar ciertos parámetros a través de la línea de comandos, de la forma (exe representa el nombre del ejecutable): 
    > ./exe NP NC BC NPP NCC  
- Donde los parámetros son:
    - NP : Número de productores.
    - NC : Número de consumidores.
    - BC : Tamaño del buffer.
    - NPP : Número de productos a producir por cada productor.
    - NCC : Número de productos a consumir por cada consumidor.

- Los parámetros deben tener ciertos requisitos o el programa no se ejecutará:
    - La cantidad de parametros ingresada debe ser exactamente 5.
    - Los parametros deben ser estrictamente positivos.
    - La cantidad total de productos (NP\*NPP) no debe superar la cantidad que puedan consumir los consumidores en total más la capacidad en buffer (NC\*NCC + BC).
    -  La cantidad total de productos (NP\*NPP) no puede ser menor a la cantidad de productos que puedan consumir los consumidores en total (NC\*NCC)

---

##### **Para la implementación de la solución, se escribe los registros o logs en archivos de texto. Al ejecutar el programa, se debe esperar a que este termine. El tiempo de ejecución dependerá de los parámetros ingresados. Los resultados estarán en 2 archivos de texto llamados "productores.txt" y "consumidores.txt"** 
